﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleConsoleLogs
{
    /// <summary>
    /// A simple class that allows consistent logging of messages to the console.
    /// </summary>
    public class ConsoleLogger
    {
        /// <summary>
        /// Logs a line to the console.
        /// </summary>
        /// <param name="line">The line to log.</param>
        /// <param name="includePrettyLines">Optional parameter for pretty separators for each log message.</param>
        /// <param name="includeDate">Optional parameter to include the date in the log message.</param>
        /// <param name="includeTime">Optional parameter to include the time in the log message.</param>
        public void Log(string line, bool includePrettyLines = false, bool includeDate = false, bool includeTime = false)
        {
            if(includePrettyLines)
                Console.WriteLine("====================================");
            Console.WriteLine(line);
            if(includeDate)
                Console.WriteLine($"Date: {DateTime.Now.ToShortDateString()}");

            if(includeTime)
                Console.WriteLine($"Time: {DateTime.Now.ToShortTimeString()}");

            if(includePrettyLines)
                Console.WriteLine("====================================");
        }
    }
}
