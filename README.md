# simple-console-logs
A very simple C# library that allows you to log consistently to the console. For when log4net is too powerful!

# Install
To install this package, run the following command in the Package Manager Console: `Install-Package SimpleConsoleLogs`.

# Use
To use, make an instance of the `SimpleConsoleLogs.ConsoleLogger` object and call the `Log()` method on the instance, making sure to include the line you add as well as some other options. [View the method details](https://github.com/muyiwaolu/simple-console-logs/blob/master/SimpleConsoleLogs/ConsoleLogger.cs) for more information

